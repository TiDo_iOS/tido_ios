//
//  SettingsTableViewCell.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 01/07/2021.
//

import UIKit

protocol SettingsTableViewCellDelegate: AnyObject {
    func didTapButton(with title: String)
}

class SettingsTableViewCell: UITableViewCell {

    static let identifier = "SettingsTableViewCell"
    static func nib() -> UINib{
        return UINib(nibName: "SettingsTableViewCell", bundle: nil)
    }
    
    public func configure(with title: String){
        self.title = title
        myLabel.text = title
    }
    private var title = " "
    @IBOutlet var myLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
