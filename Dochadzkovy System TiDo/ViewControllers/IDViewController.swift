//
//  IDViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 03/07/2021.
//

import UIKit

class IDViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let alertController =  UIAlertController(title: "vyhladat podla id", message: nil, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: idField)

    }
    private func idField(textField: UITextField!){
        var idTextField = UITextField()
        idTextField = textField
        idTextField.placeholder = "id"
        print("OK")
    }

}
