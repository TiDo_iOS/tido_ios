//
//  ViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 22/06/2021.
//

import UIKit

protocol HomeViewControllerDelegate: AnyObject {
    func didTapMenuButton()
}
class HomeViewController: UIViewController {

    weak var delegate: HomeViewControllerDelegate?
    lazy var checkInVC = CheckInViewController()
    lazy var breakVC = BreakViewController()
    lazy var exitVC = ExitViewController()
    lazy var applyexitVC = ApplyedExitViewController()
    lazy var pauseVC = PauseViewController()
    lazy var settingsVC = SettingsViewController()

    let time_label = UILabel()
    
    let image = UIImage(named: "prichod2.png")
    let button = UIButton()
    
    let image_break = UIImage(named: "prestavka2.png")
    let button_break = UIButton()
    
    let image_exit = UIImage(named: "odchod2.png")
    let button_exit = UIButton()
    
    let image_aExit = UIImage(named: "sluzobny_3.png")
    let button_aExit = UIButton()
    
    let image_logo = UIImage(named: "logo2.png")
    let button_logo = UIButton()
    
    let image_pause = UIImage(named: "prerusenie2.png")
    let button_pause = UIButton()
    
    let image_stop = UIImage(named: "stop.png")
    let button_stop = UIButton()
    
    let image_start = UIImage(named: "start.png")
    let button_start = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "TiDo"
        view.backgroundColor = .white
        view.addSubview(time_label)
        view.addSubview(button)
        view.addSubview(button_break)
        view.addSubview(button_exit)
        view.addSubview(button_aExit)
        view.addSubview(button_logo)
        view.addSubview(button_pause)
        //view.addSubview(button_start)
        //view.addSubview(button_stop)
        
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.rightAnchor.constraint(equalTo: button_break.leftAnchor, constant: -1).isActive = true
        button.bottomAnchor.constraint(equalTo: time_label.topAnchor, constant: -1).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button.setBackgroundImage(image, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    
        button_break.translatesAutoresizingMaskIntoConstraints = false
        button_break.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button_break.bottomAnchor.constraint(equalTo: time_label.topAnchor, constant: -1).isActive = true
        button_break.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_break.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_break.setBackgroundImage(image_break, for: UIControl.State.normal)
        button_break.addTarget(self, action: #selector(didTapButtonBreak), for: .touchUpInside)
        
        button_exit.translatesAutoresizingMaskIntoConstraints = false
        button_exit.leftAnchor.constraint(equalTo: button_break.rightAnchor, constant: 1).isActive = true
        button_exit.bottomAnchor.constraint(equalTo: time_label.topAnchor, constant: -1).isActive = true
        button_exit.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_exit.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_exit.setBackgroundImage(image_exit, for: UIControl.State.normal)
        button_exit.addTarget(self, action: #selector(didTapButtonExit), for: .touchUpInside)
           
        button_aExit.translatesAutoresizingMaskIntoConstraints = false
        button_aExit.rightAnchor.constraint(equalTo: button_logo.leftAnchor, constant: -1).isActive = true
        button_aExit.topAnchor.constraint(equalTo: time_label.bottomAnchor, constant: 1).isActive = true
        button_aExit.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_aExit.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_aExit.setBackgroundImage(image_aExit, for: UIControl.State.normal)
        button_aExit.addTarget(self, action: #selector(didTapButtonAexit), for: .touchUpInside)

        button_logo.translatesAutoresizingMaskIntoConstraints = false
        button_logo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button_logo.topAnchor.constraint(equalTo: time_label.bottomAnchor, constant: 1).isActive = true
        button_logo.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_logo.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_logo.setBackgroundImage(image_logo, for: UIControl.State.normal)
        
        button_pause.translatesAutoresizingMaskIntoConstraints = false
        button_pause.leftAnchor.constraint(equalTo: button_logo.rightAnchor, constant: 1).isActive = true
        button_pause.topAnchor.constraint(equalTo: time_label.bottomAnchor, constant: 1).isActive = true
        button_pause.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_pause.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_pause.setBackgroundImage(image_pause, for: UIControl.State.normal)
        button_pause.addTarget(self, action: #selector(didTapButtonPause), for: .touchUpInside)
        
        //Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true) // a tu ho pouzivam
        time_label.translatesAutoresizingMaskIntoConstraints = false
        time_label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        time_label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        time_label.widthAnchor.constraint(equalToConstant: 303).isActive = true
        time_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        time_label.backgroundColor = .purple
        time_label.textAlignment = .center
        time_label.font = UIFont(name: "Arial", size: 28.0)
        time_label.text = DateFormatter.localizedString(from: NSDate() as Date,dateStyle:DateFormatter.Style.none, timeStyle: DateFormatter.Style.short)

        

        navigationController?.navigationBar.tintColor = .label
        navigationController?.navigationBar.barTintColor = .purple
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Arial", size: 23)!]
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "info.circle"), style: .done, target: self, action: #selector(didTapInfoButton))
        configureItems()
        checkSwitchState()
        
        
        
    }
    
    private func configureItems() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "list.dash"), style: .done, target: self, action: #selector(didTapMenuButton))
    }
    
    @objc func didTapButton(){
        let vc = checkInVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "TiDo"
    }
    @objc func didTapButtonBreak(){
        let vc = breakVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "TiDo"
    }
    @objc func didTapButtonExit(){
        let vc = exitVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "TiDo"
    }
    @objc func didTapButtonAexit(){
        let vc = applyexitVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "TiDo"
    }
    @objc func didTapButtonPause(){
        let vc = pauseVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: false)
        self.title = "TiDo"
    }
    @objc func didTapInfoButton(){
        let alert = UIAlertController(title: "Info", message: "Aplikácia TiDo slúži na zaznamenanie dochádzkových údajov, ktoré sa on-line dostanú do Vašej databázy a sprístupníme Vám ich cez web rozhranie.Radi Vám poskztneme bližšie informácie o dochádzkovom systéme TiDo a jeho funkcionalite a kvalite a tak nás neváhajte kontaktovať na \n tido@tido.sk \n alebo \n+421 907 206 011", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:{ action in}))
        present(alert, animated: true)
    }

    @objc func didTapMenuButton() {
        delegate?.didTapMenuButton()
    }
    
    @objc func didTapButtonStart(){
        let alert = UIAlertController(title: "Chyba", message: "Zákazy nie sú dostupné.\nVyskúšajte neskôr", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:{ action in}))
        present(alert, animated: true)
    }
    
    @objc func didTapButtonStop(){
        let alert = UIAlertController(title: "Chyba", message: "Zákazy nie sú dostupné.\nVyskúšajte neskôr", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:{ action in}))
        present(alert, animated: true)
    }
    
    // to je ten loop
   /* @objc func updateTime(){
        let time_label = UILabel()
        
        view.addSubview(time_label)
        time_label.translatesAutoresizingMaskIntoConstraints = false
        time_label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        time_label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        time_label.widthAnchor.constraint(equalToConstant: 303).isActive = true
        time_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        time_label.backgroundColor = .purple
        time_label.textAlignment = .center
        time_label.font = UIFont(name: "Arial", size: 28.0)
        time_label.text = DateFormatter.localizedString(from: NSDate() as Date,dateStyle:DateFormatter.Style.none, timeStyle: DateFormatter.Style.short)
        
    }
    */
    func showButtons(){
        view.addSubview(button_start)
        view.addSubview(button_stop)
        
        button_start.translatesAutoresizingMaskIntoConstraints = false
        button_start.centerXAnchor.constraint(equalTo: button_aExit.centerXAnchor).isActive = true
        button_start.topAnchor.constraint(equalTo: button_aExit.bottomAnchor, constant: 1).isActive = true
        button_start.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_start.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_start.setBackgroundImage(image_start, for: UIControl.State.normal)
        button_start.addTarget(self, action: #selector(didTapButtonStart), for: .touchUpInside)
            
        button_stop.translatesAutoresizingMaskIntoConstraints = false
        button_stop.centerXAnchor.constraint(equalTo: button_pause.centerXAnchor).isActive = true
        button_stop.topAnchor.constraint(equalTo: button_pause.bottomAnchor, constant: 1).isActive = true
        button_stop.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_stop.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button_stop.setBackgroundImage(image_stop, for: UIControl.State.normal)
        button_stop.addTarget(self, action: #selector(didTapButtonStop), for: .touchUpInside)
        print("appeare")
        }
    
    func removeButton(){
        button_stop.removeFromSuperview()
        button_start.removeFromSuperview()
        print("remove")
    }
}

extension HomeViewController: SettingsViewControllerDelegate{    
    func checkSwitchState() {
        let VC = SettingsViewController()
        
        if(VC.userDefaults.bool(forKey: VC.switchScreenValue)){
            VC.switchScreen.setOn(true, animated: true)
            UIApplication.shared.isIdleTimerDisabled = true
        }
        else{
            VC.switchScreen.setOn(false, animated: true)
            UIApplication.shared.isIdleTimerDisabled = false
        }
        
        if(VC.userDefaults.bool(forKey: VC.switchZakazkyValue)){
            VC.switchZakazky.setOn(true, animated: true)
            showButtons()
            print("ok home")
        }
        else{
            VC.switchZakazky.setOn(false, animated: true)
            removeButton()
            print("No home")
            
        }

    }
}
