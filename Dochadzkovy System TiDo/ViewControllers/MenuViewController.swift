//
//  MenuViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 23/06/2021.
//

import UIKit

protocol MenuViewControllerDelegate: AnyObject {
    func didSelect(menuItem: MenuViewController.MenuOptions)
    
}
class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var delegate: MenuViewControllerDelegate?
    
    enum MenuOptions: String, CaseIterable{
        case apply = "Žiadanky"
        case checkin = "Moja Dochádzka"
        case plan = "Môj plán"
        case settings = "Nastavenia"
        case write = "Napíšte nám"
        case home = "Domov"
        case vop = "VOP"
        case gdpr = "GDPR"
        
        var imageName: String {
            switch self {
            case .apply:
                return "scroll"
            case .checkin:
                return "calendar.badge.clock"
            case .plan:
                return "calendar"
            case .settings:
                return "gear"
            case .write:
                return "pencil"
            case .home:
                return "house"
            case .vop:
                return "note.text"
            case .gdpr:
                return "lock"
            }
        }
    }

    private let tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = nil
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    
    //let whiteColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        view.backgroundColor = .purple
        tableView.isScrollEnabled = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: view.safeAreaInsets.top + 44, width: view.bounds.size.width, height: 400)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOptions.allCases.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = MenuOptions.allCases[indexPath.row].rawValue
        cell.imageView?.image = UIImage(systemName: MenuOptions.allCases[indexPath.row].imageName)
        cell.imageView?.tintColor = .label
        //cell.textLabel?.textColor = .white
        cell.contentView.backgroundColor = .purple
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = MenuOptions.allCases[indexPath.row]
        delegate?.didSelect(menuItem: item)
    }

}
