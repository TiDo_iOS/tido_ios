//
//  testViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 29/06/2021.
//

import UIKit

class testViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        let button = UIButton(frame: CGRect(x:90, y: 200, width: view.frame.size.width, height: 80))
        button.backgroundColor = .white
        //button.contentEdgeInsets = UIEdgeInsets(top: 0, left: -60, bottom: 0, right: 0);
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.gray, for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial", size: 30)
        button.setTitle("Manazer 1", for: .normal)
        view.addSubview(button)
        
        let button2 = UIButton(frame: CGRect(x:90, y: 290, width: view.frame.size.width, height: 80))
        button2.backgroundColor = .white
        button2.contentHorizontalAlignment = .left
        //button2.contentEdgeInsets = UIEdgeInsets(top: 0, left: -60, bottom: 0, right: 0);
        button2.setTitleColor(UIColor.gray, for: .normal)
        button2.titleLabel?.font = UIFont(name: "Arial", size: 30)
        button2.setTitle("Zamestnanec 1", for: .normal)
        view.addSubview(button2)
        
        let imageName = "employee.png"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image)
        
        imageView.frame = CGRect(x:5, y:200, width: 80, height: 80)
        view.addSubview(imageView)
        
        let imageName2 = "employee.png"
        let image2 = UIImage(named: imageName2)
        let imageView2 = UIImageView(image: image2)
        
        imageView2.frame = CGRect(x:5, y:290, width: 80, height: 80)
        view.addSubview(imageView2)
        
        
        
    }
    @objc func updateTime(){
        let time_label = UILabel()
        
        view.addSubview(time_label)
        time_label.translatesAutoresizingMaskIntoConstraints = false
        time_label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        time_label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        time_label.widthAnchor.constraint(equalToConstant: 303).isActive = true
        time_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        time_label.backgroundColor = .purple
        time_label.textAlignment = .center
        time_label.font = UIFont(name: "Arial", size: 28.0)
        time_label.text = DateFormatter.localizedString(from: NSDate() as Date,dateStyle:DateFormatter.Style.none, timeStyle: DateFormatter.Style.medium)
        
    }
}
