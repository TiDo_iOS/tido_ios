//
//  MyTableViewCell.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 01/07/2021.
//

import UIKit

protocol MyTableViewCellDelegate: AnyObject {
    func didTapButton(with title: String)
}
class MyTableViewCell: UITableViewCell {

    weak var delegate: MyTableViewCellDelegate?
    static let identifier = "MyTableViewCell"
    static func nib() -> UINib{
        return UINib(nibName: "MyTableViewCell", bundle: nil)
    }
    
    public func configure(with title: String, imageName: String){
        self.title =  title
        button.setTitle(title, for: .normal)
        myImageView.image = UIImage(named: "employee.png")
        
    }
    
    @IBOutlet var myImageView: UIImageView!
    @IBOutlet var button: UIButton!
    private var title: String = ""
    @IBAction func didTapButton() {
        delegate?.didTapButton(with: title)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        button.setTitleColor(.gray, for: .normal)
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        button.titleLabel?.font = UIFont(name: "Arial", size: 25)
    }
    
}
