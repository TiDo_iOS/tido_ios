//
//  InfoViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 24/06/2021.
//

import UIKit

class InfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Info"
        
        let labelInfo = UILabel(frame: CGRect(x: 20, y: view.safeAreaInsets.top - 60, width: 390, height: 600))
        view.addSubview(labelInfo)
        labelInfo.textAlignment = .justified
        labelInfo.lineBreakMode = .byWordWrapping
        labelInfo.numberOfLines = 0
        labelInfo.text = "Aplikácia TiDo slúži na zaznamenanie dochádzkových údajov, ktoré sa on-line dostanú do Vašej databázy a sprístupníme Vám ich cez web rozhranie. \nRadi Vám poskztneme bližšie informácie o dochádzkovom systéme TiDo a jeho funkcionalite a kvalite a tak nás neváhajte kontaktovať na tido@tido.sk alebo +421 907 206 011"
    }
    
}
