//
//  PauseViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 29/06/2021.
//

import UIKit

class PauseViewController: UIViewController {

    lazy var changeVC = ChangeViewController()
    lazy var privateVC = PrivateViewController()
    lazy var drVC = DrViewController()
    lazy var drbreakVC = DrBreakViewController()
    lazy var smokeVC = SmokeViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Prerušenia"
       // let formatter: DateFormatter = {
       //     let formatter = DateFormatter()
       ///     formatter.timeZone = .current
       //     formatter.locale = .current
       //     formatter.dateFormat = "HH:mm"
       //     return formatter
       // }()
        
        //let date = Date()
        let time_label = UILabel()
        view.addSubview(time_label)
        
        let image_change = UIImage(named: "volno.png")
        let button_change = UIButton()
        view.addSubview(button_change)
        button_change.setBackgroundImage(image_change, for: UIControl.State.normal)
        button_change.addTarget(self, action: #selector(didTapButtonChange), for: .touchUpInside)
        
        let image_shop = UIImage(named: "sukromne.png")
        let button_shop = UIButton()
        view.addSubview(button_shop)
        button_shop.setBackgroundImage(image_shop, for: UIControl.State.normal)
        button_shop.addTarget(self, action: #selector(didTapButtonShop), for: .touchUpInside)
        
        let image_dr = UIImage(named: "lekar.png")
        let button_dr = UIButton()
        view.addSubview(button_dr)
        button_dr.setBackgroundImage(image_dr, for: UIControl.State.normal)
        button_dr.addTarget(self, action: #selector(didTapButtonDr), for: .touchUpInside)
        
        let image_dr_break = UIImage(named: "lekar_sprevadzanie.png")
        let button_dr_break = UIButton()
        view.addSubview(button_dr_break)
        button_dr_break.setBackgroundImage(image_dr_break, for: UIControl.State.normal)
        button_dr_break.addTarget(self, action: #selector(didTapButtonDrBreak), for: .touchUpInside)
        
        let image_smoke = UIImage(named: "fajcenie.png")
        let button_smoke = UIButton()
        view.addSubview(button_smoke)
        button_smoke.setBackgroundImage(image_smoke, for: UIControl.State.normal)
        button_smoke.addTarget(self, action: #selector(didTapButtonSmoke), for: .touchUpInside)
        
        
        //button_change.translatesAutoresizingMaskIntoConstraints = false
       // button_change.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //button_change.bottomAnchor.constraint(equalTo: time_label.topAnchor, constant: -1).isActive = true
        //button_change.widthAnchor.constraint(equalToConstant: 100).isActive = true
        //button_change.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        button_shop.translatesAutoresizingMaskIntoConstraints = false
        button_shop.rightAnchor.constraint(equalTo: button_dr.leftAnchor, constant: -1).isActive = true
        button_shop.topAnchor.constraint(equalTo: time_label.bottomAnchor, constant: 1).isActive = true
        button_shop.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_shop.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        button_dr.translatesAutoresizingMaskIntoConstraints = false
        button_dr.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button_dr.topAnchor.constraint(equalTo: time_label.bottomAnchor, constant: 1).isActive = true
        button_dr.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_dr.heightAnchor.constraint(equalToConstant: 100).isActive = true

        button_dr_break.translatesAutoresizingMaskIntoConstraints = false
        button_dr_break.leftAnchor.constraint(equalTo: button_dr.rightAnchor, constant: 1).isActive = true
        button_dr_break.topAnchor.constraint(equalTo: time_label.bottomAnchor, constant: 1).isActive = true
        button_dr_break.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_dr_break.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
       
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        time_label.translatesAutoresizingMaskIntoConstraints = false
        time_label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        time_label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        time_label.widthAnchor.constraint(equalToConstant: 302).isActive = true
        time_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        time_label.backgroundColor = .purple
        time_label.textAlignment = .center
        time_label.font = UIFont(name: "Arial", size: 28.0)
        time_label.text = DateFormatter.localizedString(from: NSDate() as Date,dateStyle:DateFormatter.Style.none, timeStyle: DateFormatter.Style.medium)
        
        //if sender.isOn{
        button_change.translatesAutoresizingMaskIntoConstraints = false
        button_change.rightAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button_change.bottomAnchor.constraint(equalTo: time_label.topAnchor, constant: -1).isActive = true
        button_change.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_change.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        button_smoke.translatesAutoresizingMaskIntoConstraints = false
        button_smoke.leftAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button_smoke.bottomAnchor.constraint(equalTo: time_label.topAnchor, constant: -1).isActive = true
        button_smoke.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button_smoke.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
    }
    @objc private func didTapButtonChange(){
        let vc = changeVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "Prerušenie"
    }
    @objc private func didTapButtonShop(){
        let vc = privateVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "Prerušenie"
    }
    @objc private func didTapButtonDr(){
        let vc = drVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "Prerušenie"
    }
    @objc private func didTapButtonDrBreak(){
        let vc = drbreakVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "Prerušenie"
    }
    @objc private func didTapButtonSmoke(){
        let vc = smokeVC
        self.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent:self)
        self.navigationController?.pushViewController(vc, animated: true)
        self.title = "Prerušenie"
        
    }
    @objc func updateTime(){
        let time_label = UILabel()
        
        view.addSubview(time_label)
        time_label.translatesAutoresizingMaskIntoConstraints = false
        time_label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        time_label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        time_label.widthAnchor.constraint(equalToConstant: 303).isActive = true
        time_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        time_label.backgroundColor = .purple
        time_label.textAlignment = .center
        time_label.font = UIFont(name: "Arial", size: 28.0)
        time_label.text = DateFormatter.localizedString(from: NSDate() as Date,dateStyle:DateFormatter.Style.none, timeStyle: DateFormatter.Style.medium)
        
    }
}
