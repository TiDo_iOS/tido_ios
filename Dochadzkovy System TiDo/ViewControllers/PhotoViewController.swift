//
//  PhotoViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 02/07/2021.
//

import UIKit

class PhotoViewController: UIViewController {

    let imageView = UIImageView(frame: CGRect(x:50, y:50, width: 300, height: 400))
    let button = UIButton(frame: CGRect(x:70, y: 500, width: 300, height: 50))
    let picker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(imageView)
        view.addSubview(button)
        button.backgroundColor = .red
        button.setTitle("take picture", for: .normal)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        imageView.backgroundColor = .secondarySystemBackground

        
        
        
        
    }
    @objc func didTapButton(){
        //let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate = self
        present(picker, animated: true)
    }
    
}
extension PhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        guard  let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        imageView.image = image
    }
    
}

