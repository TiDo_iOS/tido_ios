//
//  SmokeViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 03/07/2021.
//

import UIKit

class SmokeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = nil
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    let data = ["Manažér 1", "Zamestnanec 1", "Zamestnanec 2", "Zamestnanec 3","Zamestnanec 4", "Zamestnanec 5", "Zamestnanec 6"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        view.backgroundColor = .white
        title = "Zvolte zamestnanca"
        tableView.register(MyTableViewCell.nib(), forCellReuseIdentifier: MyTableViewCell.identifier)
        tableView.dataSource = self
        tableView.backgroundColor = .white
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        //tableView.bottomAnchor.constraint(equalTo: field.topAnchor).isActive = true
        tableView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: view.frame.size.height).isActive = true
        configure()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customCell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier, for: indexPath) as! MyTableViewCell
        customCell.selectionStyle = UITableViewCell.SelectionStyle.none
        customCell.configure(with: data[indexPath.row], imageName: "employee.png")
        customCell.delegate = self
        return customCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 82
    }
    func configure(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "magnifyingglass"), style: .done, target: self, action: #selector(didTapButtonLupa))
    }
    @objc func didTapButtonLupa(){
        
        let alertController =  UIAlertController(title: "Vyhladať podľa id", message: nil, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: idField)
        present(alertController, animated: true)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alertController] (_) in
                                                    let textField = alertController?.textFields![0]; print("\(String(describing: textField?.text))")}))
        alertController.addAction(UIAlertAction(title: "Späť", style: .cancel, handler:{ action in}))
        
    }
    
    func idField(textField: UITextField!){
        var idTextField = UITextField()
        idTextField = textField
        idTextField.placeholder = "id"
        idTextField.delegate = self
        idTextField.autocorrectionType = .no
        idTextField.autocapitalizationType = .words
        idTextField.keyboardType = .asciiCapableNumberPad
    }

}
extension SmokeViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text{
            print("\(text)")
            textField.endEditing(true)
        }
        return true
    }
}

extension SmokeViewController: MyTableViewCellDelegate{
    func didTapButton(with title: String) {
        print("\(title)")
        //tableView.addSubview(photo.view)
        //tableView.frame = photo.view.frame

        
    }
}

