//
//  ContainerViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 23/06/2021.
//
import MessageUI
import SafariServices
import UIKit

class ContainerViewController: UIViewController, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate {

    enum MenuSate {
        case opened
        case closed
    }
    
    private var menuState: MenuSate = .closed
    let menuVC = MenuViewController()
    let homeVC = HomeViewController()
    var navVC: UINavigationController?
    lazy var applyVC = ApplyViewController()
    lazy var checkVC = CheckViewController()
    lazy var planVC = PlanViewController()
    lazy var settingsVC = SettingsViewController()
    lazy var writeVC = WriteViewController()
    lazy var vopVC = VopViewController()
    lazy var gdprVC = GdprViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChildVCs()
    }
    
    private func addChildVCs() {
        menuVC.delegate = self
        addChild(menuVC)
        view.addSubview(menuVC.view)
        menuVC.didMove(toParent: self)
        
        homeVC.delegate = self
        let navVC = UINavigationController(rootViewController: homeVC)
        addChild(navVC)
        view.addSubview(navVC.view)
        navVC.didMove(toParent: self)
        self.navVC = navVC
    }
}

extension ContainerViewController: HomeViewControllerDelegate{
    func didTapMenuButton(){
        toggleMenu(completion: nil)
            
    }
    
    func toggleMenu(completion: (() -> Void)?) {
        switch menuState{
        case .closed:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut) {
                self.navVC?.view.frame.origin.x = self.homeVC.view.frame.size.width - 150
            } completion: { [weak self] done in
                if done{
                    self?.menuState = .opened
                }
            }
        case .opened:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut) {
                self.navVC?.view.frame.origin.x = 0
            } completion: { [weak self] done in
                if done{
                    self?.menuState = .closed
                    DispatchQueue.main.async {
                        completion?()
                    }
                }
            }
        }
    }
}
    
extension ContainerViewController: MenuViewControllerDelegate {
    func didSelect(menuItem: MenuViewController.MenuOptions) {
        toggleMenu(completion: nil)
            switch menuItem{
            case .apply:
                self.addApply()
            case .checkin:
                self.addCheckIn()
            case .plan:
                self.addPlan()
            case .settings:
                self.addSettings()
            case .write:
                self.addWrite()
            case .home:
                self.toHome()
            case .vop:
                self.addVOP()
            case .gdpr:
                self.addGDPR()
            }
        }

    func addApply(){
        let vc = applyVC
        homeVC.addChild(vc)
        homeVC.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: homeVC)
        homeVC.navigationController?.pushViewController(vc, animated: true)
        homeVC.title = "TiDo"
        
    }

    func addCheckIn(){
        //let vc = checkVC
        //homeVC.addChild(vc)
        //homeVC.view.addSubview(vc.view)
        //vc.view.frame = view.frame
        //vc.didMove(toParent: homeVC)
        //homeVC.navigationController?.pushViewController(vc, animated: true)
        //homeVC.title = "TiDo"
        let alert = UIAlertController(title: "Nedostupná služba", message: "Služba je nedostupná v demo verzii", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:{ action in}))
        present(alert, animated: true)
    }
    
    func addPlan(){
        //let vc = planVC
        //homeVC.addChild(vc)
        //homeVC.view.addSubview(vc.view)
        //vc.view.frame = view.frame
        //vc.didMove(toParent: homeVC)
        //homeVC.navigationController?.pushViewController(vc, animated: true)
        //homeVC.title = "TiDo"
        let alert = UIAlertController(title: "Nedostupná služba", message: "Služba je nedostupná v demo verzii", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:{ action in}))
        present(alert, animated: true)
    }
    
    func addSettings(){
        let vc = settingsVC
        homeVC.addChild(vc)
        homeVC.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: homeVC)
        homeVC.navigationController?.pushViewController(vc, animated: true)
        homeVC.title = "TiDo"
    }
    
    func addWrite(){
        sendMail()
    }

    func toHome(){
        didTapToLogo()
    
    }
    func addVOP(){
        let vc = vopVC
        homeVC.addChild(vc)
        homeVC.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: homeVC)
        homeVC.navigationController?.pushViewController(vc, animated: true)
        homeVC.title = "TiDo"
    }
    func addGDPR(){
        let vc = gdprVC
        homeVC.addChild(vc)
        homeVC.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: homeVC)
        homeVC.navigationController?.pushViewController(vc, animated: true)
        homeVC.title = "TiDo"
    }
    func sendMail(){
        let actionsheet = UIAlertController(title:"Napíšte nám", message:"Pošlite email cez", preferredStyle: .actionSheet)
        actionsheet.addAction(UIAlertAction(title:"Gmail", style: .default, handler: {action in self.didTapButton()}))
        actionsheet.addAction(UIAlertAction(title:"Email", style: .default, handler: {action in self.didTapButton()}))
        actionsheet.addAction(UIAlertAction(title:"Zrusit", style: .destructive, handler: {action in}))
        present(actionsheet, animated: true)
    }
    
    @objc private func didTapButton(){
        if MFMailComposeViewController.canSendMail(){
            let vc = MFMailComposeViewController()
            vc.delegate = self
            vc.setSubject("contact us")
            vc.setToRecipients(["codewima@gmail.com"])
            vc.setMessageBody("<h1>Dobry den</h1>", isHTML: true)
            present(UINavigationController(rootViewController:vc), animated: true)
        }
    else{
        guard let url = URL(string: "https://www.google.com") else{
            return
    }
    
    let vc = SFSafariViewController(url: url)
    present(vc, animated:true)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    @objc func didTapToLogo(){
        let url = NSURL(string: "https://tido.sk")
        UIApplication.shared.open(url as! URL, options:[:], completionHandler:nil)
    }
}
