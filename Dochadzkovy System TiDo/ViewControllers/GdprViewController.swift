//
//  GdprViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 24/06/2021.
//

import UIKit

class GdprViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "GDPR"
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        view.addSubview(scrollView)
        
        let headerLabel = UITextView()
        scrollView.addSubview(headerLabel)
        
        let textLabel = UITextView()
        scrollView.addSubview(textLabel)
        
        let licenceLabel = UITextView()
        scrollView.addSubview(licenceLabel)
        
        let poskLabel = UITextView()
        scrollView.addSubview(poskLabel)
        
        let subjLabel = UITextView()
        scrollView.addSubview(subjLabel)
        
        let subj2Label = UITextView()
        scrollView.addSubview(subj2Label)
        
        let subj3Label = UITextView()
        scrollView.addSubview(subj3Label)
        
        let subj4Label = UITextView()
        scrollView.addSubview(subj4Label)
        
        let subj5Label = UITextView()
        scrollView.addSubview(subj5Label)
        
        let subj6Label = UITextView()
        scrollView.addSubview(subj6Label)
        
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant: 100).isActive = true
        headerLabel.font = UIFont.boldSystemFont(ofSize: 18)
        headerLabel.text = "TiDo všeobecné obchodné podmienky \nINFORMÁCIA O SPRACÚVANÍ OSOBNÝCH ÚDAJOV"
        headerLabel.isEditable = false
        headerLabel.isScrollEnabled = false
        headerLabel.textAlignment = .center
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor).isActive = true
        textLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        textLabel.heightAnchor.constraint(equalToConstant: 120).isActive = true
        textLabel.isEditable = false
        textLabel.isScrollEnabled = false
        textLabel.font = UIFont(name: "Arial", size: 15.0)
        textLabel.textAlignment = .justified
        textLabel.text = "uzatvorená podľa čl. 28 ods. 3 Nariadenia Európskeho parlamentu a Rady (EÚ) 2016/679 z 27.4.2016\no ochrane fyzických osôb pri spracúvaní osobných\n údajov a o voľnom pohybe takýchto údajov,ktorým sa zrušuje smernica 95/46/ES (všeobecné nariadenie o ochrane údajov)"
        
        licenceLabel.translatesAutoresizingMaskIntoConstraints = false
        licenceLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        licenceLabel.topAnchor.constraint(equalTo: textLabel.bottomAnchor).isActive = true
        licenceLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        licenceLabel.heightAnchor.constraint(equalToConstant: 70).isActive = true
        licenceLabel.isEditable = false
        licenceLabel.isScrollEnabled = false
        licenceLabel.textAlignment = .center
        licenceLabel.font = UIFont.boldSystemFont(ofSize: 18)
        licenceLabel.text = "I.\nPoskytovateľ licencie"
        
        poskLabel.translatesAutoresizingMaskIntoConstraints = false
        poskLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        poskLabel.topAnchor.constraint(equalTo: licenceLabel.bottomAnchor).isActive = true
        poskLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        poskLabel.heightAnchor.constraint(equalToConstant: 190).isActive = true
        poskLabel.font = UIFont(name: "Arial", size: 15.0)
        poskLabel.isEditable = false
        poskLabel.isScrollEnabled = false
        poskLabel.text = "obchodné meno:                    TiDo s.r.o.\nso sídlom:                              Jarná 17, 056 01 \n                                               Gelnica\nIČO:                                       47 453 753 \nzapísaná:                               v obchodnom \n                                               registri Okresného \n                                               súdu Košice I, \n                                               Oddiel: Sro, \n                                               vložka č.: 34171/V"
        
        
        subjLabel.translatesAutoresizingMaskIntoConstraints = false
        subjLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subjLabel.topAnchor.constraint(equalTo: poskLabel.bottomAnchor).isActive = true
        subjLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        subjLabel.heightAnchor.constraint(equalToConstant: 70).isActive = true
        subjLabel.font = UIFont.boldSystemFont(ofSize: 18)
        subjLabel.isEditable = false
        subjLabel.isScrollEnabled = false
        subjLabel.textAlignment = .center
        subjLabel.text = "II.\n Predmet"
        
        subj2Label.translatesAutoresizingMaskIntoConstraints = false
        subj2Label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subj2Label.topAnchor.constraint(equalTo: subjLabel.bottomAnchor).isActive = true
        subj2Label.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        subj2Label.heightAnchor.constraint(equalToConstant: 300).isActive = true
        subj2Label.font = UIFont(name: "Arial", size: 15)
        subj2Label.isEditable = false
        subj2Label.isScrollEnabled = false
        subj2Label.textAlignment = .justified
        subj2Label.text = "V súvislosti s podmienkami spracovania a ochranou osobných údajov v zmysle Nariadenia Európskeho parlamentu a Rady (EÚ) 2016/679 z 27.4.2016 o ochrane fyzických osôb pri spracúvaní osobných údajov a o voľnom pohybe takýchto údajov, poskytovateľ licencie k Dochádzkovému systému TiDo je v zmysle zákona o ochrane osobných údajov príjemcom. V záujme jednoznačného označenia zmluvných strán sa klient, ktorý sa rozhodne zakúpiť licenciu považuje za prevádzkovateľa, ktorý spracúva osobné údaje vo vlastnom mene, a poskytovateľ licencie sa považuje za sprostredkovateľa, ktorý spracúva osobné údaje na základe zdokumentovaných pokynov prevádzkovateľa a v jeho mene."
        
       
        subj3Label.translatesAutoresizingMaskIntoConstraints = false
        subj3Label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subj3Label.topAnchor.constraint(equalTo: subj2Label.bottomAnchor).isActive = true
        subj3Label.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        subj3Label.heightAnchor.constraint(equalToConstant: 70).isActive = true
        subj3Label.font = UIFont.boldSystemFont(ofSize: 18)
        subj3Label.isEditable = false
        subj3Label.isScrollEnabled = false
        subj3Label.textAlignment = .center
        subj3Label.text = "II.\n Predmet"
        
        subj4Label.translatesAutoresizingMaskIntoConstraints = false
        subj4Label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subj4Label.topAnchor.constraint(equalTo: subj3Label.bottomAnchor).isActive = true
        subj4Label.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        subj4Label.heightAnchor.constraint(equalToConstant: 1450).isActive = true
        subj4Label.isEditable = false
        subj4Label.isScrollEnabled = false
        subj4Label.font = UIFont(name: "Arial", size: 15)
        subj4Label.text = "1. Pri podpise licenčnej zmluvy sa zmluvné strany dohodli, že spracúvanie osobných údajov bude sprostredkovateľ vykonávať podľa nasledujúcich pokynov:\n   a) Sprostredkovateľ je oprávnený spracúvať osobné údaje v mene prevádzkovateľa odo dňa uzavretia existujúcej zmluvy.\n   \n   b) Sprostredkovateľ je oprávnený spracúvať osobné údaje na účely uvedené v existujúcej zmluve alebo na účely vyplývajúce z tejto zmluvy, tzn. na účely zaznamenávania dochádzkových údajov zamestnancov\n   \n   c) Predmetom spracúvania sú nasledujúce kategórie osobných údajov:\n     1) Meno a priezvisko zamestnanca\n     2) Osobné číslo zamestnanca\n     3) Dátum narodenia zamestnanca\n     4) E-mail zamestnanca\n     5) Fotografia zamestnanca\n   \n   d) Spracúvanie zahŕňa nasledujúce spracovateľské operácie:\n     6) Získavanie,\n     7) Zhromažďovanie,\n     8) Preskupovanie,\n     9) Nahliadanie,\n     10) Sprístupňovanie\n   \n   e) Spracúvanie sa týka nasledujúcich kategórií dotknutých osôb:\n     1) Zamestnanci prevádzkovateľa\n     2) Bývalí zamestnanci zamestnávateľa\n   \n   f) Osobné údaje sa spracovávajú po dobu dohodnutú s prevádzkovateľom podľa účelu a podľa potreby uchovávať jednotlivé údaje, minimálne však po zákonnú lehotu na uchovávanie osobných  údajov pri spracovávaní dochádzky zamestnancov.\n   \n2. So zreteľom na najnovšie poznatky, náklady na vykonanie opatrení a na povahu, rozsah, kontext a účely spracúvania, ako aj na riziká s rôznou pravdepodobnosťou a závažnosťou pre práva a slobody fyzických osôb, sprostredkovateľ prijal primerané technické a organizačné opatrenia s cieľom zaistiť úroveň bezpečnosti primeranú tomuto riziku. Pri posudzovaní primeranej úrovne bezpečnosti sa prihliada predovšetkým na riziká, ktoré predstavuje spracúvanie, a to najmä v dôsledku náhodného alebo nezákonného zničenia, straty, zmeny, neoprávneného poskytnutia osobných údajov, ktoré sa prenášajú, uchovávajú alebo inak spracúvajú, alebo neoprávneného prístupu k takýmto údajom.\n  \n3. Sprostredkovateľ\n    \n   a) má zato, že prevádzkovateľ má splnenú informačnú povinnosť voči svojim zamestnancom o spracovávaní osobných údajov sprostredkovateľom, ktorú vie kedykoľvek písomné preukázať;\n\n   b) zabezpečí, aby sa osoby oprávnené spracúvať osobné údaje zaviazali zachovávať dôvernosť informácií;\n\n   c) pomáha prevádzkovateľovi zabezpečiť plnenie povinností podľa článkov 32 až 36 nariadenia s prihliadnutím na povahu spracúvania a informácie dostupné sprostredkovateľovi;\n \n4. Sprostredkovateľ potvrdzuje, že podľa požiadaviek článku 37 až 39 Nariadenia určil zodpovednú osobu (ďalej len „Zodpovedná osoba“) pre agendu ochrany osobných údajov:\n\nMeno, priezvisko:    Michaela Nemčíková\nFunkcia:   Zodpovednú osobu pre ochranu osobných údajov\nTelefón:   0901921436\nE-mail:   zodpovednaosoba@tido.sk"
        
        
        subj5Label.translatesAutoresizingMaskIntoConstraints = false
        subj5Label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subj5Label.topAnchor.constraint(equalTo: subj4Label.bottomAnchor).isActive = true
        subj5Label.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        subj5Label.heightAnchor.constraint(equalToConstant: 70).isActive = true
        subj5Label.font = UIFont.boldSystemFont(ofSize: 18)
        subj5Label.isEditable = false
        subj5Label.isScrollEnabled = false
        subj5Label.textAlignment = .center
        subj5Label.text = "III.\nZáverečné ustanovenia"
        
        subj6Label.translatesAutoresizingMaskIntoConstraints = false
        subj6Label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subj6Label.topAnchor.constraint(equalTo: subj5Label.bottomAnchor).isActive = true
        subj6Label.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        subj6Label.heightAnchor.constraint(equalToConstant: 600).isActive = true
        subj6Label.isEditable = false
        subj6Label.isScrollEnabled = false
        subj6Label.font = UIFont(name: "Arial", size: 15)
        subj6Label.text = "1. Tieto podmienky nadobúdajú platnosť dňom podpísania licenčnej zmluvy oboma zmluvnými stranami. Táto zmluva sa uzatvára na dobu platnosti licenčnej zmluvy.\n\n2. Otázky výslovne neupravené touto zmluvou sa riadia príslušnými právnymi predpismi, najmä nariadením, zákonom o ochrane osobných údajov a v súlade s ustanovením § 261 ods. 1 zákona č. 513/1991 Zb. Obchodný zákonník v znení neskorších predpisov (ďalej len „Obchodný zákonník“), aj ustanoveniami Obchodného zákonníka.\n\n3. Obe strany tejto zmluvy zhodne deklarujú, že táto zmluva vyjadruje obsah ich dohody o podmienkach spracúvania a ochrane osobných údajov a ako taká nahrádza všetky predchádzajúce písomné či ústne dojednania.\n\n V Gelnici, dňa 25.05.2018 \n\nTiDo s.r.o."
            
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 2840)
    }
}

