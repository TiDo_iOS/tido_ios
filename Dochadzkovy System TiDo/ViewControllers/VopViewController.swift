//
//  VopViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 24/06/2021.
//

import UIKit

class VopViewController: UIViewController, UITextViewDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "VOP"

        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        view.addSubview(scrollView)
        
        let header = UITextView()
        scrollView.addSubview(header)
        
        let content = UITextView()
        scrollView.addSubview(content)
        
        let headerTwo = UITextView()
        scrollView.addSubview(headerTwo)
        
        let contentTwo = UITextView()
        scrollView.addSubview(contentTwo)
        
        let headerThree = UITextView()
        scrollView.addSubview(headerThree)
        
        let contentThree = UITextView()
        scrollView.addSubview(contentThree)
        
        let headerFour = UITextView()
        scrollView.addSubview(headerFour)
        
        let contentFour = UITextView()
        scrollView.addSubview(contentFour)
        
        let headerFive = UITextView()
        scrollView.addSubview(headerFive)
        
        let contentFive = UITextView()
        scrollView.addSubview(contentFive)
        
        let headerSix = UITextView()
        scrollView.addSubview(headerSix)
        
        let contentSix = UITextView()
        scrollView.addSubview(contentSix)
        
        let headerSeven = UITextView()
        scrollView.addSubview(headerSeven)
        
        let contentSeven = UITextView()
        scrollView.addSubview(contentSeven)
        
        let headerEight = UITextView()
        scrollView.addSubview(headerEight)
        
        let contentEight = UITextView()
        scrollView.addSubview(contentEight)
        
        
        header.translatesAutoresizingMaskIntoConstraints = false
        header.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        header.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        header.heightAnchor.constraint(equalToConstant: 110).isActive = true
        
        header.text = "Všeobecné podmienky používania internetovej stránky www.tido.sk a mobilnej\naplikácie TiDo – Dochádzkový systém"
        header.textAlignment = .center
        header.isScrollEnabled = false
        header.isEditable = false
        header.font = UIFont.boldSystemFont(ofSize: 18)
    
        content.translatesAutoresizingMaskIntoConstraints = false
        content.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        content.topAnchor.constraint(equalTo: header.bottomAnchor).isActive = true
        content.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        content.heightAnchor.constraint(equalToConstant: 220).isActive = true
        content.isEditable = false
        content.isScrollEnabled = false
        content.textAlignment = .justified
        content.isUserInteractionEnabled = true
        content.font = UIFont(name: "Arial", size: 15)
        content.text =  "Spoločnosť TiDo s.r.o., so sídlom Jarná 17, 056 01 Gelnica, IČO: 47 453 753, zapísaná v Obchodnom registri vedenom Okresným súdom Košice I, Oddiel: Sro, Vložka č.:  34171/V DIČ: 2023919227, IČ DPH: SK2023919227 ako prevádzkovateľ internetovej stránky www.tido.sk a mobilnej aplikácie TiDo – Dochádzkový systém dostupnej pre OS Android vydáva tieto Všeobecné podmienky používania Internetovej stránky a Mobilnej aplikácie (v texte ďalej aj „Podmienky“):"
      
        headerTwo.translatesAutoresizingMaskIntoConstraints = false
        headerTwo.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerTwo.topAnchor.constraint(equalTo: content.bottomAnchor).isActive = true
        headerTwo.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerTwo.heightAnchor.constraint(equalToConstant: 50).isActive = true
        headerTwo.textAlignment = .center
        headerTwo.font = UIFont.boldSystemFont(ofSize: 18)
        headerTwo.isEditable = false
        headerTwo.isScrollEnabled = false
        headerTwo.text = "Článok 1\nZákladné ustanovenia"
        
        contentTwo.translatesAutoresizingMaskIntoConstraints = false
        contentTwo.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentTwo.topAnchor.constraint(equalTo: headerTwo.bottomAnchor).isActive = true
        contentTwo.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentTwo.heightAnchor.constraint(equalToConstant: 1790).isActive = true
        contentTwo.isEditable = false
        contentTwo.isScrollEnabled = false
        contentTwo.font = UIFont(name: "Arial", size: 15)
        contentTwo.text = "1. Vymedzenie pojmov:\n a)Prevádzkovateľ – obchodná spoločnosť TiDo s.r.o., so sídlom Jarná 17, 056 01 Gelnica, IČO: 47 453 753, zapísaná v Obchodnom registri vedenom Okresným súdom Košice I, Oddiel: Sro, Vložka č.:  34171/V DIČ: 2023919227, IČ DPH: SK2023919227 (v texte Ďalej aj „Prevádzkovateľ“),\n b) Používateľ – je každá právnická alebo fyzická osoba, ktorá je používateľom mobilnej aplikácie a ktorá je zároveň pri registrácii na stránke www.tido.sk alebo v mobilnej aplikácii uvedená ako „Používateľ.\n c) Mobilná aplikácia TiDo – je dochádzkový systém TiDo (v texte ďalej aj „mobilná aplikácia“), ktorého účelom je evidovanie dochádzky zamestnancov, plánovanie a evidencia pracovných zmien, dovoleniek, práceneschopnosti, náhradného voľna, prekážok v práci, výpočet dochádzkových údajov zamestnancov, vyhotovenie fotografie, evidencia a vyhodnocovanie iných údajov prístupných v systéme  na základe jeho konfigurácie a ponuky,\n d) Demoverzia – je verejne prístupná verzia mobilnej aplikácie alebo internetovej stránky, ktorá je pre Používateľov bezplatná a Prevádzkovateľ nezaručuje dostupnosť všetkých jej funkcií a vlastností, pod čim možno rozumieť, okrem iných aj to, že údaje v Demoverzii môžu byť prepísané iným Používateľom. Prístup na Internetovú stránku a prístup k Mobilnej aplikácii a ich používanie sú bezplatné v demo verzii.\ne) Plná verzia – Prístup na Internetovú stránku a prístup k Mobilnej aplikácii a ich Používanie pre plnú verziu podliehajú licenčnej zmluve medzi Poskytovateľom a Záujemcom, kde sa stanovia podmienky používania pre Záujemcu a vytvorí sa vlastná databáza a vlastný prístup k nej.\n f) Internetová stránka - sprístupnenie dochádzkového systému prostredníctvom web stránky, ako subdomény, za použitia webového prehliadača, najmä Google Chrome alebo Mozzilla a ich nadstavieb,\n g) tretia osoba/dotknutá osoba -  označuje akýkoľvek iný tretí subjekt bez ohľadu na to či ide o  právnickú osobu alebo fyzickú osobu. Ide o všeobecné označenie osoby odlišnej od osôb zmluvných strán tejto dohody,\n h) Obsah – súbor údajov a dát nachádzajúcich sa na internetovej stránke alebo v mobilnej aplikácii, ktorých záväznosť a rozsah sa mení vzhľadom na verziu, ktorú Používateľ užíva,\n -  súbor údajov, ktoré Používateľ zadáva na internetovú stránku alebo do mibilnej aplikácie, za účelom ich užívania\n 2. Predmet úpravy Všeobecných podmienok: \n- predmetom úpravy všeobecných podmienok používania internetovej stránky a mobilnej aplikácie je úprava podmienok, za ktorých je používateľ oprávnený používať mobilnú aplikáciu v demoverzii alebo plnej verzii, internetovú stránku, určenie spôsobu ich používania, ako aj základné pravidlá ich dostupnosti. \n 3. Zvolením alebo použitím Internetovej stránky a/alebo Mobilnej aplikácie (ďalej len „Použitie“) akceptujú všetci používatelia alebo návštevníci platnosť Podmienok používania. Okamihom použitia sú pre nich podmienky záväzné a sú ich povinní dodržiavať.\n 4.  Užívatelia, ktorí nesúhlasia s týmito Podmienkami, nie sú oprávnení využívať Mobilnú aplikáciu.\n 5.  Udelenie súhlasu s podmienkami znamená poskytnutie služby, ktorej predmetom je sprístupnenie mobilnej aplikácie a jej funkcionalít v príslušnom rozsahu podliehajúcom výberu tej-ktorej verzie mobilnej aplikácie. \n 6. Ak nie je uvedené na Internetovej stránke alebo v Mobilnej aplikácii výslovne inak, obsah zverejnený na Internetovej stránke a/alebo v Mobilnej aplikácii má výlučne informačný charakter.\n 7.  Internetová stránka a Mobilná aplikácia, ako aj ich jednotlivé časti sú chránené zákonom č. 618/2003 Z. z. o autorskom práve a právach súvisiacich s autorským právom (autorský zákon) v znení neskorších predpisov (ďalej len „autorský zákon“). Výlučným vlastníkom majetkových práv autora k Internetovej stránke a Mobilnej aplikácii nie je Prevádzkovateľ. Prevádzkovateľ vyhlasuje, že je oprávnený na uverejňovanie, publikovanie ako aj sprostredkovanie dostupnosti internetovej stránky a mobilnej aplikácie. Podmienky používania Internetovej stránky a Mobilnej aplikácie určuje, vydáva a mení výhradne Prevádzkovateľ.8.Akýkoľvek neoprávnený zásah do Internetovej stránky a/alebo Mobilnej aplikácie alebo ich častí, akékoľvek neoprávnené používanie Internetovej stránky a/alebo Mobilnej aplikácie alebo ich častí, akékoľvek kopírovanie alebo napodobňovanie Internetovej stránky a/alebo Mobilnej aplikácie alebo ich častí sú v rozpore s autorským zákonom alebo inými právnymi predpismi platnými na území Slovenskej republiky, a preto sa zakazujú. Ochranná známka, dizajn, obrázky, texty, časti textov a ostatný obsah Internetovej stránky a/alebo Mobilnej aplikácie sa nesmú bez predchádzajúceho písomného súhlasu Prevádzkovateľa, meniť, kopírovať, rozmnožovať, používať, doplňovať alebo iným spôsobom používať."

        headerThree.translatesAutoresizingMaskIntoConstraints = false
        headerThree.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerThree.topAnchor.constraint(equalTo: contentTwo.bottomAnchor).isActive = true
        headerThree.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerThree.heightAnchor.constraint(equalToConstant: 90).isActive = true
        headerThree.isEditable = false
        headerThree.isScrollEnabled = false
        headerThree.font = UIFont.boldSystemFont(ofSize: 18)
        headerThree.textAlignment = .center
        headerThree.text = "Článok 2\nPráva a Povinnosti požívateľov Internetovej stránky a Mobilnej aplikácie"

        contentThree.translatesAutoresizingMaskIntoConstraints = false
        contentThree.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentThree.topAnchor.constraint(equalTo: headerThree.bottomAnchor).isActive = true
        contentThree.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentThree.heightAnchor.constraint(equalToConstant: 800).isActive = true
        contentThree.isEditable = false
        contentThree.isScrollEnabled = false
        contentThree.isUserInteractionEnabled = true
        contentThree.font = UIFont(name: "Arial", size: 15)
        contentThree.text = "1.    Používateľ má právo: \n a)    využívať Demo Internetovú stránku a Demo Mobilnú aplikáciu výhradne nie na komerčný charakter,\n b)    na bezodplatné používanie demo verzie internetovej stránky a mobilnej aplikácie,\n c)    na požiadanie prevádzkovateľa o udelenie súhlasu na iné ako v písm. a) bodu 1 tohto článku uvedené použitie, iné použitie Internetovej stránky a/alebo Mobilnej aplikácie (tak celku, ako aj ich jednotlivých častí) podlieha predchádzajúcemu schváleniu TiDo s.r.o, v súlade s autorským zákonom a spísaniu licenčnej zmluvy, \n 2.    Používateľ je povinný:\n a)    zdržať sa akéhokoľvek konania, ktorým by upravil, zmenil, či akokoľvek inak zasiahol do obsahu, funkčnosti a dostupnosti internetovej stránky a/alebo mobilnej aplikácie,\n b)    akékoľvek zásahy do technického charakteru alebo obsahu Internetovej stránky a/alebo Mobilnej aplikácie Užívateľmi sú zakázané,\n c)    v prípade sankcionovania Prevádzkovateľa za porušenie povinností požívateľov uvedených v týchto Podmienkach ako aj za porušenie všeobecne záväzných právnych predpisoch Slovenskej republiky, je používateľ povinný nahradiť takto vzniknutú škodu, \n d)    ukladať si dáta a údaje zadávané do mobilnej aplikácie a/alebo na internetovú stránku, vlastnými prostriedkami, aby na jeho strane nedošlo ku škodám, \n e)    používateľ berie na vedomie, že pri využívaní Internetovej stránky a/alebo Mobilnej aplikácie je povinný zdržať sa konania, či akéhokoľvek správania sa, ktoré by bolo v rozpore so všeobecne záväznými právnymi predpismi Slovenskej republiky a/alebo s pravidlami morálky a slušnosti, a to najmä žiadnym spôsobom nemôže: \n-    propagovať detskú pornografiu, ohrozovať fyzický, psychický, alebo morálny vývin maloletých, alebo narúšať ich duševné zdravie a emocionálny stav, odosielať príspevky s erotickým alebo pornografickým obsahom;\n-    uvádzať na internetovú stránku obsah, ktorým by akokoľvek ohrozil meno, či povesť prevádzkovateľa,"

        headerFour.translatesAutoresizingMaskIntoConstraints = false
        headerFour.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerFour.topAnchor.constraint(equalTo: contentThree.bottomAnchor).isActive = true
        headerFour.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerFour.heightAnchor.constraint(equalToConstant: 70).isActive = true
        headerFour.isEditable = false
        headerFour.isScrollEnabled = false
        headerFour.font = UIFont.boldSystemFont(ofSize: 18)
        headerFour.textAlignment = .center
        headerFour.text = "Článok 3\nPráva a Povinnosti Prevádzkovateľa"

        contentFour.translatesAutoresizingMaskIntoConstraints = false
        contentFour.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentFour.topAnchor.constraint(equalTo: headerFour.bottomAnchor).isActive = true
        contentFour.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentFour.heightAnchor.constraint(equalToConstant: 500).isActive = true
        contentFour.isEditable = false
        contentFour.isScrollEnabled = false
        contentFour.isUserInteractionEnabled = true
        contentFour.font = UIFont(name: "Arial", size: 15)
        contentFour.text = "1.    Prevádzkovateľ má právo:\n a)    kedykoľvek meniť alebo dopĺňať Internetovú stránku alebo Mobilnú aplikáciu vrátane týchto Podmienok podľa svojej potreby a obchodných zámerov,\n b)    podľa vlastného uváženia kedykoľvek zmeniť Podmienky používania,\n a)    v prípade sankcionovania Prevádzkovateľa za porušenie povinností požívateľov uvedených v týchto Podmienkach ako aj za porušenie všeobecne záväzných právnych predpisoch Slovenskej republiky, má právo na náhradu škody,\n 2.    Prevádzkovateľ neposkytuje požívateľom Demo verzie stránky a mobilnej aplikácie žiadnu záruku ani garanciu ich dostupnosti, nepretržitej funkčnosti, bezchybnej činnosti a zabezpečenia Internetovej stránky a Mobilnej aplikácie a zároveň nenesie žiadnu zodpovednosť za chyby alebo vady demo verzie stránky a/alebo mobilnej aplikácie ako ani za škodu spôsobenú ich používaním.\n 3.    Prevádzkovateľ nezodpovedá za akúkoľvek škodu, ktorá by mohla byť požívateľovi spôsobená v súvislosti s používaním Internetovej stránky a/alebo Mobilnej aplikácie.\n 4.    Pre používateľov, ktorí používajú plnú verziu internetovej stránky a mobilnej aplikácie je zabezpečená záruka nepretržitej funkčnosti."
        
        headerFive.translatesAutoresizingMaskIntoConstraints = false
        headerFive.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerFive.topAnchor.constraint(equalTo: contentFour.bottomAnchor).isActive = true
        headerFive.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerFive.heightAnchor.constraint(equalToConstant: 110).isActive = true
        headerFive.isEditable = false
        headerFive.isScrollEnabled = false
        headerFive.font = UIFont.boldSystemFont(ofSize: 18)
        headerFive.textAlignment = .center
        headerFive.text = "Článok 4\nOsobitné ustanovenia pre požívateľov, ktorí pridávajú na Internetovú stránku alebo do Mobilnej aplikácie vlastný obsah"

        contentFive.translatesAutoresizingMaskIntoConstraints = false
        contentFive.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentFive.topAnchor.constraint(equalTo: headerFive.bottomAnchor).isActive = true
        contentFive.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentFive.heightAnchor.constraint(equalToConstant: 620).isActive = true
        contentFive.isEditable = false
        contentFive.isScrollEnabled = false
        contentFive.isUserInteractionEnabled = true
        contentFive.font = UIFont(name: "Arial", size: 15)
        contentFive.text = "1.    Prevádzkovateľ umiestňuje na Internetovej stránke a v Mobilnej aplikácii aj obsah, t.j. fotografie, obrázky od Užívateľov, fyzických aj právnických osôb (ďalej len „Fotografie“).\n 2.    Prevádzkovateľ prevádzkuje Internetovú stránku a Mobilnú aplikáciu najmä za účelom evidencie dochádzky zamestnancov užívateľa. Najmä príchody, odchody, prestávky, prerušenia, ktoré sa ukladajú do vytvorenej databázy, kde ich následne spracováva Záujemca ako podklad k mzdám.\n 3.    Mobilná aplikácia umožňuje pre identifikáciu zamestnanca vytvoriť fotografiu zamestnanca, pre jeho identifikáciu. Fotografia sa uloží do databázy vytvorenej pre klienta, kde sa fotky pravidelne automaticky vymazávajú dva mesiace po ich vytvorení.\n 4.    Osobné údaje sú  spracovávané prevádzkovateľom  TiDo s.r.o., so sídlom Jarná 17, 056 01 Gelnica, IČO: 47 453 753 a spracováva ich na základe zmluvy uzatvorenej prevádzkovateľom podľa zákona č. 122/2013 Z. z. o ochrane osobných údajov a o zmene a doplnení niektorých zákonov.  Ak je potrebný súhlas dotknutej osoby pre účely používania aplikácie, ten zabezpečí  prevádzkovateľ.\n 5.    Používateľ nesmie žiadnym spôsobom poškodzovať Prevádzkovateľa a jeho dobré meno. Užívateľ nemôže na Internetovej stránke a/alebo v Mobilnej aplikácii propagovať služby iných osôb, bez ohľadu na to, či sú v konkurenčnom vzťahu k Prevádzkovateľovi.\n 6.    Používateľ sa zaväzuje, že na Internetovú stránku a/alebo v Mobilnej aplikácii nebude pridávať Fotografie, ktoré sú v rozpore s dobrými mravmi a so všeobecne záväznými právnymi predpismi Slovenskej republiky."
        
        headerSix.translatesAutoresizingMaskIntoConstraints = false
        headerSix.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerSix.topAnchor.constraint(equalTo: contentFive.bottomAnchor).isActive = true
        headerSix.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerSix.heightAnchor.constraint(equalToConstant: 70).isActive = true
        headerSix.isEditable = false
        headerSix.isScrollEnabled = false
        headerSix.font = UIFont.boldSystemFont(ofSize: 18)
        headerSix.textAlignment = .center
        headerSix.text = "Článok 5\nOchrana a používanie osobných údajov"

        contentSix.translatesAutoresizingMaskIntoConstraints = false
        contentSix.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentSix.topAnchor.constraint(equalTo: headerSix.bottomAnchor).isActive = true
        contentSix.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentSix.heightAnchor.constraint(equalToConstant: 1760).isActive = true
        contentSix.isEditable = false
        contentSix.isScrollEnabled = false
        contentSix.isUserInteractionEnabled = true
        contentSix.font = UIFont(name: "Arial", size: 15)
        contentSix.text = "1.    Používanie osobných údajov:\n a)    ak v rámci používania internetovej stránky a/alebo mobilnej aplikácie poskytne používateľ svoje osobné údaje, ktoré poskytovateľ nemôže spracúvať priamo na základe právnych predpisov platných a účinných v Slovenskej republike, ktoré upravujú oblasť ochrany a spracúvania osobných údajov, pre ktoré sa vyžaduje výslovný súhlas, súhlasom s týmito všeobecnými podmienkami dáva používateľ svoj výslovný súhlas na spracúvaním svojich osobných údajov v rozsahu titul, meno, priezvisko, e-mailová adresa, fotografický záznam a to za účelom riadneho používania mobilnej aplikácie a/alebo internetovej stránky, bez ohľadu na ich verziu.\n b)    súhlas podľa predchádzajúceho bodu tohto článku poskytuje používateľ na dobu používania internetovej stránky a/alebo mobilnej aplikácie trvania a nasledujúcich dvoch mesiacov po ukončení používania internetovej stránky a mobilnej aplikácie. zmluvného vzťahu.\n c)    používateľ zároveň potvrdzuje, že bol oboznámený so všetkými informáciami, ktoré je v zmysle Právnej úpravy osobných údajov potrebné oznámiť dotknutej osobe pred získaním jej osobných údajov, a to prostredníctvom týchto VOP a stránky www.tido.sk a v mobilnej aplikácii, \n d)    prevádzkovateľ je oprávnený začať so spracúvaním osobných údajov odo okamihu udelenie súhlasu s týmito všeobecnými podmienkami používania.\n 2.    Ochrana osobných údajov:\n a)    spracúvanie osobných údajov sa riadi príslušnými ustanoveniami zákona č. 122/2013 Z. z. o ochrane osobných údajov (ďalej len ako „ZOOU“) a jeho novelizácií,\n b)    poskytovateľ je oprávnený spracúvať osobné údaje v rozsahu potrebnom pre riadne fungovanie tej-ktorej verzie internetovej stránky a/alebo mobilnej aplikácie,\n c)    spracúvanie osobných údajov na iné účely ako sú uvedené v predchádzajúcom bode tohto článku je možné len na základe súhlasu toho-ktorého používateľa,\n d)    poskytovateľ je povinný spracúvať osobné údaje riadne, profesionálne, v súlade so ZOOU a v rozsahu a za podmienok dojednaných v tejto zmluve.\ne)    poskytovateľ je oprávnený vykonávať s osobnými údajmi nasledujúce operácie: získavanie, zhromažďovanie, zaznamenávanie, usporadúvanie, vyhľadávanie, prehliadanie, využívanie, uchovávanie, likvidácia. \n f)    poskytovateľ nie je oprávnený poskytnúť osobné údaje zamestnancov nadobúdateľa ďalej tretím osobám. Poskytovateľ je povinný chrániť osobné údaje pred ich odcudzením, stratou, poškodením, zverejnením, zmenou a rozširovaním.\n g)    používateľ súhlasom s všeobecnými podmienkami prehlasuje, že súhlasí so spracovaním osobných údajov v rozsahu uvedenom v čl. V bod 1 písm. a) podmienok,\n h)    v prípade pochybností udelenia súhlasu na spracovanie osobných údajov používateľa v zmysle čl. V bod 1 písm. a) a bodu 2 písm. g sa má za to, že súhlas bol udelený, \n 3.    Prevádzkovateľ vyhlasuje, že: \n a)    zaistí primeranú úroveň zabezpečenia Obsahu pred jeho vymazaním, stratou, neoprávneným upravovaním alebo iným poškodením, ako aj pred neoprávneným prístupom tretích osôb,\n b)    neposkytne žiadne vyhlásenia ani záruky, že ním poskytované zabezpečenie Obsahu je v súlade s povinnosťami, ktoré je potrebné dodržiavať pri    spracúvaní Obsahu spadajúceho pod špecifickú právnu úpravu v zmysle predchádzajúceho odseku týchto VOP,\n c)    Obsah nezneužije vo vlastný prospech a že ho nesprístupní tretím osobám, s výnimkou prípadov, kedy takáto povinnosť bude vyplývať zo všeobecne záväzných právnych predpisov alebo z právoplatného a vykonateľného rozhodnutia orgánu verejnej moci, alebo kedy to bude potrebné na riadne uplatnenie a ochranu práv a oprávnených záujmov prevádzkovateľa zo Zmluvy alebo v súvislosti so Zmluvou.\n 4.    V prípade, ak pri používaní mobilnej aplikácie a/alebo internetovej stránky používateľ zadá alebo priamo či nepriamo sprístupní osobné údaje tretích osôb, vyhlasujete, že disponujete všetkými súhlasmi Dotknutých osôb vo forme vyžadovanej Právnou úpravou osobných údajov, ktoré sú podľa Právnej úpravy osobných údajov potrebné na spracúvanie osobných údajov Dotknutých osôb Poskytovateľom, a to najmenej v rozsahu, za účelom a po dobu uvedenú v čl. V bode 1 a 2 týchto podmienok. Používateľ zároveň vyhlasuje, že si voči Dotknutým osobám riadne splnili informačnú povinnosť v súvislosti so spracúvaním osobných údajov Dotknutých osôb a s ich poskytnutím či sprístupnením prevádzkovateľovi. Používateľ vyhlasuje, že súhlasy Dotknutých osôb podľa tohto odseku na požiadanie poskytnete prevádzkovateľovi, a to bez zbytočného odkladu."

        headerSeven.translatesAutoresizingMaskIntoConstraints = false
        headerSeven.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerSeven.topAnchor.constraint(equalTo: contentSix.bottomAnchor).isActive = true
        headerSeven.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerSeven.heightAnchor.constraint(equalToConstant: 70).isActive = true
        headerSeven.isEditable = false
        headerSeven.isScrollEnabled = false
        headerSeven.font = UIFont.boldSystemFont(ofSize: 18)
        headerSeven.textAlignment = .center
        headerSeven.text = "Článok 6\n Osobitné ustanovenia"

        contentSeven.translatesAutoresizingMaskIntoConstraints = false
        contentSeven.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentSeven.topAnchor.constraint(equalTo: headerSeven.bottomAnchor).isActive = true
        contentSeven.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentSeven.heightAnchor.constraint(equalToConstant: 880).isActive = true
        contentSeven.isEditable = false
        contentSeven.isScrollEnabled = false
        contentSeven.isUserInteractionEnabled = true
        contentSeven.font = UIFont(name: "Arial", size: 15)
        contentSeven.text = "1.    Používateľ berie na vedomie, že\n a)    internetová stránka a mobilná aplikácia bez ohľadu na ich verziu sú určené a navrhnuté pre potreby podnikateľov,\n b)    internetová stránka a mobilná aplikácia bez ohľadu na ich verziu nie je určená pre spotrebiteľov alebo pre subjekty podliehajúce osobitnej regulácii,\n c)    že udelenie oprávnenia využívať internetovú stránku a mobilnú aplikáciu bez ohľadu na ich verziu nepredstavuje poskytovanie služieb daňových poradcov alebo účtovníkov\n d)    internetová stránka a mobilná aplikácia bez ohľadu na ich verziu nie je určená na spracúvanie Obsahu, ktorý svojou povahou spadá pod špecifickú právnu úpravu (napr. Obsah podliehajúci zákonom stanovenej povinnosti mlčanlivosti, Obsah chránený ako bankové tajomstvo, daňové tajomstvo, utajované skutočnosti a pod.),\n e)    Obsah používateľ vkladá, ukladá, vypĺňa, poskytuje na internetovú stránka a mobilnú aplikáciu bez ohľadu na ich verziu na vlastné nebezpečenstvo a zodpovednosť, \n f)    Prevádzkovateľ nenesie žiadnu zodpovednosť za Obsah. \n 2.    Používateľ vyhlasuje, že s Obsahom oprávnený nakladať, že disponujete všetkými súhlasmi na nakladanie s Obsahom v rozsahu a forme vyžadovanej príslušnými právnymi predpismi a že spracovaním Obsahu nedochádza k ohrozeniu alebo porušeniu Vašich práv alebo oprávnených záujmov ani práv či oprávnených záujmov tretích osôb. Ďalej vyhlasujete, že Vami zadané registračné údaje sú pravdivé, správne a úplné a že k registrácii Používateľa a k uzavretiu Zmluvy došlo konaním osoby oprávnenej konať za Používateľa. Vyhlásenia podľa tohto odseku VOP sa považujú za zopakované pri každom použití Aplikácie.\n 3.    Pokiaľ v dôsledku preukázania nepravdivosti akéhokoľvek vyhlásenia používateľa uvedeného v týchto všeobecných podmienkach alebo sa takým uvedené vyhlásenie stane nepravdivým a v tejto súvislosti budú voči prevádzkovateľovi uplatnené akékoľvek nároky tretích osôb alebo uložené sankcie zo strany orgánov verejnej moci, zaväzujete sa používateľ na náhradu takto vzniknutej škody v plnej miere, ako aj vrátane akýchkoľvek nákladov prevádzkovateľa vzniknutých v dôsledku uplatnenia takých nárokov alebo uloženia takých sankcií alebo v súvislosti s nimi."

        headerEight.translatesAutoresizingMaskIntoConstraints = false
        headerEight.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerEight.topAnchor.constraint(equalTo: contentSeven.bottomAnchor).isActive = true
        headerEight.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        headerEight.heightAnchor.constraint(equalToConstant: 70).isActive = true
        headerEight.isEditable = false
        headerEight.isScrollEnabled = false
        headerEight.font = UIFont.boldSystemFont(ofSize: 18)
        headerEight.textAlignment = .center
        headerEight.text = "Článok 7\nZáverečné ustanovenia"

        contentEight.translatesAutoresizingMaskIntoConstraints = false
        contentEight.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentEight.topAnchor.constraint(equalTo: headerEight.bottomAnchor).isActive = true
        contentEight.widthAnchor.constraint(equalToConstant: scrollView.frame.size.width).isActive = true
        contentEight.heightAnchor.constraint(equalToConstant: 440).isActive = true
        contentEight.isEditable = false
        contentEight.isScrollEnabled = false
        contentEight.isUserInteractionEnabled = true
        contentEight.font = UIFont(name: "Arial", size: 15)
        contentEight.text = "1.    Používanie internetovej stránky a Mobilnej aplikácie, a tieto Podmienky používania Internetovej stránky a Mobilnej aplikácie sa riadia právnymi predpismi Slovenskej republiky.\n 2.    Ak zákon neustanovuje inak, súdy Slovenskej republiky majú výhradnú právomoc a sú miestne príslušné pre akékoľvek spory vzniknuté pri používaní alebo v súvislosti s používaním Internetovej stránky a Mobilnej aplikácie, alebo s týmito Podmienkami používania.\n 3.    Používateľ vyjadruje svoj súhlas s týmito podmienkami a potvrdzuje, že sa s nimi v plnom rozsahu oboznámil, porozumel im a súhlasí s nimi.\n4.    Tieto podmienky sú platné a účinné dňom zverejnenia, t. j. odo dňa 3.8.2017. \n\n\n\nTiDo s.r.o.\n\n25.09.2017"
        
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 7580)
    }
}
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
//        let linkUrl = "https://tido.sk"
//        if (URL.absoluteString == linkUrl) {
//            UIApplication.shared.open(linkUrl as! URL, options:[:], completionHandler:nil)
//        }
//        return false
//        }
//    }

//extension  UITextView {

//  func addHyperLinksToText(originalText: String, hyperLinks: [String: String]) {
//    let style = NSMutableParagraphStyle()
//    style.alignment = .center
//    let attributedOriginalText = NSMutableAttributedString(string: originalText)
//    for (hyperLink, urlString) in hyperLinks {
//        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
//        let fullRange = NSRange(location: 0, length: attributedOriginalText.length)
//        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
//        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
//        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: fullRange)
//    }

//    self.linkTextAttributes = [
//        NSAttributedString.Key.foregroundColor:  GL_RED,
//        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
//    ]
//    self.attributedText = attributedOriginalText
//  }
//}
