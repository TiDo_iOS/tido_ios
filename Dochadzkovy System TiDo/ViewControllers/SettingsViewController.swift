//
//  SettingsViewController.swift
//  Dochadzkovy System TiDo
//
//  Created by Martin Táborský on 24/06/2021.
//

import UIKit
//import Photos

protocol SettingsViewControllerDelegate: AnyObject {
    func checkSwitchState()
}

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    weak var delegate: SettingsViewControllerDelegate?
    lazy var homeVC = HomeViewController()
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = nil
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    private let tableViewTwo: UITableView = {
        let tableTwo = UITableView()
        tableTwo.backgroundColor = nil
        tableTwo.register(UITableViewCell.self, forCellReuseIdentifier: "cell2")
        return tableTwo
    }()
    
    private let labelOne: UILabel = {
        let labelOne = UILabel()
        labelOne.text = "Customer code"
        return labelOne
    }()
    private let labelTwo: UILabel = {
        let labelTwo = UILabel()
        labelTwo.text = "Resort id"
        return labelTwo
    }()
    private let labelThree: UILabel = {
        let labelThree = UILabel()
        labelThree.text = "Setup password"
        return labelThree
    }()
    
    private let textOne: UITextField = {
        let textOne = UITextField()
        textOne.placeholder = "demo"
        return textOne
    }()
    private let textTwo: UITextField = {
        let textTwo = UITextField()
        textTwo.placeholder = "id"
        return textTwo
    }()
    private let textThree: UITextField = {
        let textThree = UITextField()
        textThree.placeholder = "password"
        return textThree
    }()
    
    var options = ["Stále zapnutý display", "Zobraziť dialóg pred ukončením", "Aktivovať zákazky", "Triediť zákazky od Z k A", "Jednoduchý mód", "Jednoduchý mód 2", "Vlastné údalosťi", "Fajčenie", "Vyhľadať iba podľa ID", "Povoliť otáčanie obrazovky"]
    
    var optionsTwo = ["Enable Camera", "Allow Smoking", "Allow requests", "Allow my attendance", "Enable my schedule", " Allow orders", "Sort orders from Z to A"]
    
    let userDefaults = UserDefaults.standard
    let switchScreenValue = "switchScrenn"
    let switchScreen = UISwitch()
    
    let switchDialogValue = "switchDialog"
    let switchDialog = UISwitch()

    let switchZakazkyValue = "switchZakazky"
    let switchZakazky = UISwitch()

    let switchZtoAValue = "switchZtoA"
    let switchZtoA = UISwitch()
    
    let switchModeOneValue = "switchModeOne"
    let switchModeOne = UISwitch()
    
    let switchModeTwoValue = "switchModeTwo"
    let switchModeTwo = UISwitch()
    
    let switchCustomValue = "switchCustom"
    let switchCustom = UISwitch()
    
    let switchSmokeValue = "switchSmoke"
    let switchSmoke = UISwitch()
    
    let switchIdValue = "switchId"
    let switchId = UISwitch()
    
    let switchRotateValue = "switchRotate"
    let switchRotate = UISwitch()
    
    let switchCameraValue = "switchCamera"
    let switchCamera = UISwitch()
    
    let switchAllowSmokeValue = "switchAllowSmoke"
    let switchAllowSmoke = UISwitch()
    
    let switchRequestsValue = "switchRequests"
    let switchRequests = UISwitch()
    
    let switchAttendanceValue = "switchAttendance"
    let switchAttendance = UISwitch()
    
    let switchScheduleValue = "switchSchedule"
    let switchSchedule = UISwitch()
    
    let switchOrderValue = "switchOrder"
    let switchOrder = UISwitch()
    
    let switchSortValue = "switchSort"
    let switchSort = UISwitch()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Nastavenia"
        
        let menuItems = ["BASIC", "PERMISSIONS","ADVANCED"]
        let menu = UISegmentedControl(items: menuItems)
        view.addSubview(menu)
        menu.selectedSegmentIndex = 0
        menu.translatesAutoresizingMaskIntoConstraints = false
        menu.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        menu.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        menu.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        menu.layer.cornerRadius = 5.0
        menu.backgroundColor = .white
        menu.selectedSegmentTintColor = .purple
        menu.addTarget(self, action: #selector(didChangeColor(_:)), for: .valueChanged)
        
        view.addSubview(labelOne)
        view.addSubview(labelTwo)
        view.addSubview(labelThree)
        view.addSubview(textOne)
        view.addSubview(textTwo)
        view.addSubview(textThree)
        
        labelOne.translatesAutoresizingMaskIntoConstraints = false
        labelOne.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 40).isActive = true
        labelOne.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
        labelOne.widthAnchor.constraint(equalToConstant: 145).isActive = true
        labelOne.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        labelTwo.translatesAutoresizingMaskIntoConstraints = false
        labelTwo.topAnchor.constraint(equalTo: labelOne.bottomAnchor).isActive = true
        labelTwo.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
        labelTwo.widthAnchor.constraint(equalToConstant: 200).isActive = true
        labelTwo.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        labelThree.translatesAutoresizingMaskIntoConstraints = false
        labelThree.topAnchor.constraint(equalTo: labelTwo.bottomAnchor).isActive = true
        labelThree.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
        labelThree.widthAnchor.constraint(equalToConstant: 200).isActive = true
        labelThree.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        textOne.translatesAutoresizingMaskIntoConstraints = false
        textOne.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 45).isActive = true
        textOne.leftAnchor.constraint(equalTo: labelOne.rightAnchor).isActive = true
        textOne.widthAnchor.constraint(equalToConstant: 150).isActive = true
        textOne.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textOne.delegate = self
        textOne.autocorrectionType = .no
        textOne.autocapitalizationType = .words
        textOne.setUnderLine()
      
        textTwo.translatesAutoresizingMaskIntoConstraints = false
        textTwo.topAnchor.constraint(equalTo: textOne.bottomAnchor).isActive = true
        textTwo.centerXAnchor.constraint(equalTo: labelTwo.rightAnchor, constant: 20).isActive = true
        textTwo.widthAnchor.constraint(equalToConstant: 150).isActive = true
        textTwo.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textTwo.setUnderLine()
        textTwo.delegate = self
        textTwo.autocorrectionType = .no
        textTwo.autocapitalizationType = .words
  
        textThree.translatesAutoresizingMaskIntoConstraints = false
        textThree.topAnchor.constraint(equalTo: textTwo.bottomAnchor).isActive = true
        textThree.centerXAnchor.constraint(equalTo: labelThree.rightAnchor, constant: 20).isActive = true
        textThree.widthAnchor.constraint(equalToConstant: 150).isActive = true
        textThree.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textThree.setUnderLine()
        textThree.delegate = self
        textThree.autocorrectionType = .no
        textThree.autocapitalizationType = .words
        
        checkSwitchState()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int?
        
        if tableView == self.tableView{
            count = options.count
        }
        if tableView == self.tableViewTwo{
            count = optionsTwo.count
        }
        return count!
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customCell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.identifier, for: indexPath) as! SettingsTableViewCell
        customCell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if tableView == self.tableView{
            if indexPath.row == 0 {
                let myswitch = switchScreen
                myswitch.addTarget(self, action: #selector(didChangeSwitchScreen(_:)), for: .valueChanged)
                customCell.accessoryView = myswitch
                customCell.configure(with:"Stále zapnutý display")
                return customCell
            }
            if indexPath.row == 1{
                let mySwitch = switchDialog
                customCell.configure(with: "Zobraziť dialóg pred ukončením")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchDialog(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 2{
                customCell.configure(with: "Aktivovať zákazky")
                switchZakazky.addTarget(self, action: #selector(didChangeSwitchStartStop(_:)), for: .valueChanged)
                customCell.accessoryView = switchZakazky
                return customCell
            }
            if indexPath.row == 3{
                let mySwitch = switchZtoA
                customCell.configure(with: "Triediť zákazky od Z k A")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchZtoA(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 4{
                let mySwitch = switchModeOne
                customCell.configure(with: "Jednoduchý mód")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchModeOne(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 5{
                let mySwitch = switchModeTwo
                customCell.configure(with: "Jednoduchý mód 2")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchModeTwo(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 6{
                let mySwitch = switchCustom
                customCell.configure(with: "Vlastné údalosťi")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchCustom(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 7{
                let mySwitch = switchSmoke
                customCell.configure(with: "Fajčenie")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchSmoke(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 8{
                let mySwitch = switchId
                customCell.configure(with: "Vyhľadať iba podľa ID")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchId(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 9{
                let mySwitch = switchRotate
                customCell.configure(with: "Povoliť otáčanie obrazovky")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchRotate(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
        }
        
        if tableView == self.tableViewTwo{
            if indexPath.row == 0{
                let mySwitch = switchCamera
                customCell.configure(with: "Enable Camera")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchCamera(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 1{
                let mySwitch = switchAllowSmoke
                customCell.configure(with: "Allow Smoking")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchAllowSmoke(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 2{
                let mySwitch = switchRequests
                customCell.configure(with: "Allow requests")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchRequests(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 3{
                let mySwitch = switchAttendance
                customCell.configure(with: "Allow my attendance")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchAttendance(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 4{
                let mySwitch = switchSchedule
                customCell.configure(with: "Enable my schedule")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchSchedule(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 5{
                let mySwitch = switchOrder
                customCell.configure(with: "Allow orders")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchOrder(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
            if indexPath.row == 6{
                let mySwitch = switchSort
                customCell.configure(with: "Sort orders from Z to A")
                mySwitch.addTarget(self, action: #selector(didChangeSwitchSort(_:)), for: .valueChanged)
                customCell.accessoryView = mySwitch
                return customCell
            }
        }
        return customCell
    }

    @objc func didChangeColor(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            tableView.removeFromSuperview()
            tableViewTwo.removeFromSuperview()
            view.backgroundColor = .white
            view.addSubview(labelOne)
            view.addSubview(labelTwo)
            view.addSubview(labelThree)
            view.addSubview(textOne)
            view.addSubview(textTwo)
            view.addSubview(textThree)
            labelOne.translatesAutoresizingMaskIntoConstraints = false
            labelOne.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 40).isActive = true
            labelOne.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
            labelOne.widthAnchor.constraint(equalToConstant: 145).isActive = true
            labelOne.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            labelTwo.translatesAutoresizingMaskIntoConstraints = false
            labelTwo.topAnchor.constraint(equalTo: labelOne.bottomAnchor).isActive = true
            labelTwo.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
            labelTwo.widthAnchor.constraint(equalToConstant: 200).isActive = true
            labelTwo.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            labelThree.translatesAutoresizingMaskIntoConstraints = false
            labelThree.topAnchor.constraint(equalTo: labelTwo.bottomAnchor).isActive = true
            labelThree.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
            labelThree.widthAnchor.constraint(equalToConstant: 200).isActive = true
            labelThree.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            textOne.translatesAutoresizingMaskIntoConstraints = false
            textOne.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 45).isActive = true
            textOne.leftAnchor.constraint(equalTo: labelOne.rightAnchor).isActive = true
            textOne.widthAnchor.constraint(equalToConstant: 150).isActive = true
            textOne.heightAnchor.constraint(equalToConstant: 40).isActive = true
            textOne.setUnderLine()
            
            textTwo.translatesAutoresizingMaskIntoConstraints = false
            textTwo.topAnchor.constraint(equalTo: textOne.bottomAnchor).isActive = true
            textTwo.centerXAnchor.constraint(equalTo: labelTwo.rightAnchor, constant: 20).isActive = true
            textTwo.widthAnchor.constraint(equalToConstant: 150).isActive = true
            textTwo.heightAnchor.constraint(equalToConstant: 40).isActive = true
            textTwo.setUnderLine()
            
            textThree.translatesAutoresizingMaskIntoConstraints = false
            textThree.topAnchor.constraint(equalTo: textTwo.bottomAnchor).isActive = true
            textThree.centerXAnchor.constraint(equalTo: labelThree.rightAnchor, constant: 20).isActive = true
            textThree.widthAnchor.constraint(equalToConstant: 150).isActive = true
            textThree.heightAnchor.constraint(equalToConstant: 40).isActive = true
            textThree.setUnderLine()
        }
        if sender.selectedSegmentIndex == 1{
            tableView.removeFromSuperview()
            labelOne.removeFromSuperview()
            labelTwo.removeFromSuperview()
            labelThree.removeFromSuperview()
            textOne.removeFromSuperview()
            textTwo.removeFromSuperview()
            textThree.removeFromSuperview()
            
            view.addSubview(tableViewTwo)
            tableViewTwo.register(SettingsTableViewCell.nib(), forCellReuseIdentifier: SettingsTableViewCell.identifier)
            tableViewTwo.delegate = self
            tableViewTwo.dataSource = self
            tableViewTwo.backgroundColor = .white
            tableViewTwo.isScrollEnabled = false
            tableViewTwo.translatesAutoresizingMaskIntoConstraints = false
            tableViewTwo.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 45).isActive = true
            tableViewTwo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            tableViewTwo.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
            tableViewTwo.heightAnchor.constraint(equalToConstant: 300).isActive = true
        }
        
        if sender.selectedSegmentIndex == 2{
            view.backgroundColor = .white
            labelOne.removeFromSuperview()
            labelTwo.removeFromSuperview()
            labelThree.removeFromSuperview()
            textOne.removeFromSuperview()
            textTwo.removeFromSuperview()
            textThree.removeFromSuperview()
            tableViewTwo.removeFromSuperview()
            view.addSubview(tableView)
            tableView.register(SettingsTableViewCell.nib(), forCellReuseIdentifier: SettingsTableViewCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.backgroundColor = .white
            tableView.isScrollEnabled = false
            tableView.translatesAutoresizingMaskIntoConstraints = false
            tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 45).isActive = true
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            tableView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
            tableView.heightAnchor.constraint(equalToConstant: 420).isActive = true
        }
    }
    
    @objc func didChangeSwitchScreen(_ sender: UISwitch){
         if sender.isOn{
             UIApplication.shared.isIdleTimerDisabled = true
             userDefaults.setValue(true, forKey: switchScreenValue)
        }
         else{
             UIApplication.shared.isIdleTimerDisabled = false
             userDefaults.setValue(false, forKey: switchScreenValue)
        }
     }
     
     @objc func didChangeSwitchDialog(_ sender: UISwitch){
         if sender.isOn{
             userDefaults.setValue(true, forKey: switchDialogValue)
         }
         else{
             userDefaults.setValue(false, forKey: switchDialogValue)
         }
     }
    
    @objc func didChangeSwitchStartStop(_ sender: UISwitch){
        if sender.isOn{
            homeVC.showButtons()
            userDefaults.setValue(true, forKey: switchZakazkyValue)
            
            print("ok settings")
        }
        else{
            userDefaults.setValue(false, forKey: switchZakazkyValue)
            homeVC.removeButton()
            print("no settings")
            
        }
    }
 
    @objc func didChangeSwitchZtoA(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchZtoAValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchZtoAValue)
        }
    }
    
    @objc func didChangeSwitchModeOne(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchModeOneValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchModeOneValue)
        }
    }
    
    @objc func didChangeSwitchModeTwo(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchModeTwoValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchModeTwoValue)
        }
    }
    
    @objc func didChangeSwitchCustom(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchCustomValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchCustomValue)
        }
    }
    
    @objc func didChangeSwitchSmoke(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchSmokeValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchSmokeValue)
        }
    }
    
    @objc func didChangeSwitchId(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchIdValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchIdValue)
        }
    }
    
    @objc func didChangeSwitchRotate(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchRotateValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchRotateValue)
        }
    }
    
    @objc func didChangeSwitchCamera(_ sender: UISwitch){
        //var photos = PHPhotoLibrary.authorizationStatus()
        
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchCameraValue)
            //photos = .authorized
            //print("access allowed")
            
        }
        else{
            userDefaults.setValue(false, forKey: switchCameraValue)
            //photos = .denied
            //print("access denied")
            
        }
    }
    
    @objc func didChangeSwitchAllowSmoke(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchAllowSmokeValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchAllowSmokeValue)
        }
    }
    
    @objc func didChangeSwitchRequests(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchRequestsValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchRequestsValue)
        }
    }

    @objc func didChangeSwitchAttendance(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchAttendanceValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchAttendanceValue)
        }
    }
    
    @objc func didChangeSwitchSchedule(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchScheduleValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchScheduleValue)
        }
    }
    
    @objc func didChangeSwitchOrder(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchOrderValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchOrderValue)
        }
    }
    
    @objc func didChangeSwitchSort(_ sender: UISwitch){
        if sender.isOn{
            userDefaults.setValue(true, forKey: switchSortValue)
        }
        else{
            userDefaults.setValue(false, forKey: switchSortValue)
        }
    }

    func checkSwitchState(){
        delegate?.checkSwitchState()
        if(userDefaults.bool(forKey: switchScreenValue)){
            switchScreen.setOn(true, animated: true)
            UIApplication.shared.isIdleTimerDisabled = true
        }
        else{
            switchScreen.setOn(false, animated: true)
            UIApplication.shared.isIdleTimerDisabled = false
        }
        if(userDefaults.bool(forKey: switchDialogValue)){
            switchDialog.setOn(true, animated: true)
            
        }
        
        else{switchDialog.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchZakazkyValue)){
            switchZakazky.setOn(true, animated: true)
                print("ok settings value")
            homeVC.showButtons()

        }
        else{
            switchZakazky.setOn(false, animated: true)
            homeVC.removeButton()
            print("no settings value")
        }
        
        if (userDefaults.bool(forKey: switchZtoAValue)){
            switchZtoA.setOn(true, animated: true)
        }
        else{switchZtoA.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchModeOneValue)){
            switchModeOne.setOn(true, animated: true)
        }
        else{switchModeOne.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchModeTwoValue)){
            switchModeTwo.setOn(true, animated: true)
        }
        else{switchModeTwo.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchCustomValue)){
            switchCustom.setOn(true, animated: true)
        }
        else{switchCustom.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchSmokeValue)){
            switchSmoke.setOn(true, animated: true)
        }
        else{switchSmoke.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchIdValue)){
            switchId.setOn(true, animated: true)
        }
        else{switchId.setOn(false, animated: true)}
        
        if(userDefaults.bool(forKey: switchRotateValue)){
            switchRotate.setOn(true, animated: true)
        }
        else{switchRotate.setOn(false, animated: true)}
        
        if(userDefaults.bool(forKey: switchCameraValue)){
            switchCamera.setOn(true, animated: true)
        }
        else{switchCamera.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchAllowSmokeValue)){
            switchAllowSmoke.setOn(true, animated: true)
        }
        else{switchAllowSmoke.setOn(false, animated: true)}
        
        if (userDefaults.bool(forKey: switchRequestsValue)){
            switchRequests.setOn(true, animated: true)
        }
        else{switchRequests.setOn(false, animated: true)}
        
        if(userDefaults.bool(forKey: switchAttendanceValue)){
            switchAttendance.setOn(true, animated: true)
        }
        else{switchAttendance.setOn(false, animated: true)}
        
        if(userDefaults.bool(forKey: switchScheduleValue)){
            switchSchedule.setOn(true, animated: true)
        }
        else{switchSchedule.setOn(false, animated: true)}
        
        if(userDefaults.bool(forKey: switchOrderValue)){
            switchOrder.setOn(true, animated: true)
        }
        else{switchOrder.setOn(false, animated: true)}
        
        if(userDefaults.bool(forKey: switchSortValue)){
            switchSort.setOn(true, animated: true)
        }
        else{switchSort.setOn(false, animated: true)}
    }
}

extension UITextField {
    func setUnderLine() {
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.purple.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width - 10, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

extension SettingsViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text{
            print("\(text)")
            textField.endEditing(true)
        }
        return true
    }
}
